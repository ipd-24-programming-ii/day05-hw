package day05_hw;

class Dancer{
	protected String name;
	protected int age;
	public void dance() {
		System.out.println("this is from parent dancer class");
	}
}

class ElectricBoogieDancer extends Dancer{
	public void dance() {
		System.out.println("this is from child ElectricBoogieDancer class");
	}
	
}

class BreakDancer extends Dancer{
	public void dance() {
		System.out.println("this is from child BreakDancer class");
	}
}

public class Day05_hw {
	
	public static void main(String[] args) {
		Dancer [] dancerList = new Dancer[] {new Dancer(), new ElectricBoogieDancer(), new BreakDancer()};
		for (int i = 0; i < dancerList.length; i++) {
			dancerList[i].dance();
		}
	}
}
